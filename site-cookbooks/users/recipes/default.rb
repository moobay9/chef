#
# Cookbook Name:: users
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

# sudo
bash "wheeluser enable" do
	code <<-EOC
		sed -i "s/\#auth.*required.*pam_wheel.so/auth\t\trequired\tpam_wheel.so/g" /etc/pam.d/su
		sed -i "s/\# %wheel\tALL=(ALL)\tALL/%wheel\tALL=(ALL)\tALL/g" /etc/sudoers
	EOC
end

# sshd
if node['users']['root_login'] == "false"
  bash "sshd config" do
    code <<-EOC
      sed -i "s/\#PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
      sed -i "s/PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config
    EOC
  end
end

service "sshd" do
  action [:enable, :start]
  supports :status => true, :restart => true
  subscribes :restart, "bash[sshd config]"
end

# group + user
ids = data_bag_item('users', 'default')['users']

ids.each do |u|
  group u['name'] do
    gid u['gid']
    action :create
  end

  user u['name'] do
    home u['home']
    shell u['shell']
    gid u['gid']
    uid u['uid']
    password u['password']
    manage_home true
    action :create
  end

  group "wheel_" + u['name'] do
    group_name "wheel"
    action :modify
    members u['name']
    append true
  end

  directory u['home'] + '/.ssh' do
    owner u['uid']
    group u['gid']
    mode 0700
    action :create
  end

  file u['home'] + '/.ssh/authorized_keys' do
    owner u['uid']
    group u['gid']
    mode 0600
    content u['ssh_key']
    action :create
  end

end
