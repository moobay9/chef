iptables Cookbook
=================
This cookbook makes your favorite breakfast sandwich.

Requirements
------------
none

Attributes
----------

Usage
-----
#### iptables::default

e.g.
Just include `iptables` in your node's `run_list`:

```json
  "iptables": {
    "rule": [
      {"port": 21},
      {"port": 80},
      {"udp": true, "port": 53},
      {"source": "127.0.0.1/24", "port": 443},
      {"access": "REJECT", "port": 443}
    ]
  }
```

Contributing
------------

License and Authors
-------------------
