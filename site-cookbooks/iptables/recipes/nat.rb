#
# Cookbook Name:: iptables
# Recipe:: default
#
# Copyright 2014, Funaffect Inc.
#
# All rights reserved - Do Not Redistribute
#

package "iptables" do
  action :install
end

if node["iptables"]["rule"].empty?

  template "/etc/sysconfig/iptables" do
    source "nat.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :list => node['iptables']['rule']
    )
    notifies :reload, 'service[iptables]'
  end

else

  template "/etc/sysconfig/iptables" do
    source "iptables.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :list => node['iptables']['rule']
    )
    notifies :reload, 'service[iptables]'
  end

  template "/etc/sysconfig/iptables-nat" do
    source "nat.erb"
    owner "root"
    group "root"
    mode 0644
    variables(
      :list => node['iptables']['rule']
    )
    notifies :reload, 'service[iptables]'
  end

  execute "merge iptables" do
    command <<-EOC
      cat /etc/sysconfig/iptables-nat >> /etc/sysconfig/iptables
    EOC
    action :run
    not_if "test \"1\" = `grep '*nat' /etc/sysconfig/iptables | wc -l`"
  end

end

service "iptables" do
  action [ :enable, :start]
  supports :status => true, :restart => true
  subscribes :restart, resources(:template => "/etc/sysconfig/iptables")
end
