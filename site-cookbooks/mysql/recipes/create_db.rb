#
# Cookbook Name:: mysql
# Recipe:: create_db
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

schema = node['mysql']['schema']

schema.each do |sm|
  execute "create_db " + sm do
    command "/usr/bin/mysql -u root -p#{node['mysql']['root_password']} -e \"CREATE DATABASE #{sm} CHARACTER SET #{node['mysql']['default_character']}\""
    action :run
    not_if "/usr/bin/mysql -u root -p#{node['mysql']['root_password']} -D #{sm}"
  end
end
