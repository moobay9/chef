#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

%w{httpd httpd-devel mod_ssl}.each do |pkg|
  package pkg do
    action :install
  end
end

template "/etc/httpd/conf/httpd.conf" do
  source "httpd.conf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end

template "/etc/httpd/conf.d/ssl.conf" do
  source "ssl.conf.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end

service "httpd" do
  action [ :enable, :start]
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => "/etc/httpd/conf/httpd.conf")
end
