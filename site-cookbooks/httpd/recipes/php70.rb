#
# Cookbook Name:: httpd
# Recipe:: php
#
# Copyright 2017, Funaffect
#
# All rights reserved - Do Not Redistribute
#

case node['platform']
when "amazon"
  %w{php70 php70-devel php70-gd php70-mbstring php70-bcmath php70-mysqlnd php70-pdo php70-mcrypt php70-zip}.each do |pkg|
    package pkg do
      action :install
    end
  end
when "centos", "redhat"
  include_recipe "yum-epel"
  include_recipe "yum-remi"

  # Need Remi Repository
  %w{php70-php php70-php-devel php70-php-gd php70-php-mbstring php70-php-bcmath php70-php-mysqlnd php70-php-xml php70-php-mcrypt php70-php-pecl-zip}.each do |pkg|
    package pkg do
      action :install
    end
  end

  link '/usr/bin/php' do
    to '/usr/bin/php70'
  end
end


template "/etc/php.ini" do
  source "php55.ini.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end
