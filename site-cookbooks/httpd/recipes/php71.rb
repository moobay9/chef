#
# Cookbook Name:: httpd
# Recipe:: php
#
# Copyright 2017, Funaffect
#
# All rights reserved - Do Not Redistribute
#

case node['platform']
when "amazon"
  %w{php71 php71-devel php71-gd php71-mbstring php71-bcmath php71-mysqlnd php71-pdo php71-mcrypt php71-zip}.each do |pkg|
    package pkg do
      action :install
    end
  end
when "centos", "redhat"
  include_recipe "yum-epel"
  include_recipe "yum-remi"

  # Need Remi Repository
  %w{php71-php php71-php-devel php71-php-gd php71-php-mbstring php71-php-bcmath php71-php-mysqlnd php71-php-xml php71-php-mcrypt php71-php-pecl-zip}.each do |pkg|
    package pkg do
      action :install
    end
  end

  link '/usr/bin/php' do
    to '/usr/bin/php71'
  end
end


template "/etc/php.ini" do
  source "php55.ini.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end
