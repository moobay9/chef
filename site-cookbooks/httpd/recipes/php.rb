#
# Cookbook Name:: httpd
# Recipe:: php
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

%w{php php-devel php-gd php-mbstring php-pear php-bcmath php-mysql php-pdo php-xml php-mcrypt}.each do |pkg|
  package pkg do
    action :install
  end
end

template "/etc/php.ini" do
  source "php.ini.erb"
  owner "root"
  group "root"
  mode 0644
  notifies :reload, 'service[httpd]'
end
