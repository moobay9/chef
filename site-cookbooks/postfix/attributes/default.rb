default["postfix"]["mydomain"] = ""
default["postfix"]["relay_domains"] = []
default["postfix"]["mynetworks"] = []
default["postfix"]["transport_maps"] = []
default["postfix"]["blacklist_mailfrom"] = []

default["postfix"]["smarthost_files"] = [
                                          "blacklist_mailfrom", 
                                          "check_ns", 
                                          "check_sender_info", 
                                          "header_checks", 
                                          "permit_client_nots25r", 
                                          "postgrey_whitelist_clients", 
                                          "postgrey_whitelist_recipients", 
                                          "prepend_client", 
                                          "reject_client_blackip", 
                                          "reject_helo", 
                                          "reject_mx", 
                                          "reject_ns", 
                                          "transport_maps", 
                                          "whitelist_client", 
                                          "whitelist_recipient"
                                        ]

default["postfix"]["hash_files"] = [
                                      "reject_client_blackip",
                                      "reject_mx",
                                      "reject_ns",
                                      "transport_maps",
                                      "whitelist_client",
                                      "whitelist_recipient"
                                    ]

default["postfix"]["postgrey"]["inet"]        = "60000"
default["postfix"]["postgrey"]["tarpit"]      = "125"
default["postfix"]["postgrey"]["retry-count"] = "2"
default["postfix"]["postgrey"]["delay"]       = "3600"

default["postfix"]["spf"]["debugLevel"] = "1"
default["postfix"]["spf"]["seed_only"] = "1"
default["postfix"]["spf"]["hello_reject"] = "SPF_Not_Pass"
default["postfix"]["spf"]["mail_from_reject"] = "Fail"
default["postfix"]["spf"]["perm_error_reject"] = "False"
default["postfix"]["spf"]["temp_error_defer"] = "False"
default["postfix"]["spf"]["skip_addresses"] = []