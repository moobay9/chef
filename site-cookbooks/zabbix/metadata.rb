name             'zabbix'
maintainer       'Funaffect Inc.'
maintainer_email 'm.oobayashi@funaffect.jp'
license          'All rights reserved'
description      'Installs/Configures zabbix'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.2.0'

depends 'yum'
