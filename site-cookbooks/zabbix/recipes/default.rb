#
# Cookbook Name:: zabbix
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

# zabbix-agent

case node['platform']
when "centos", "redhat"
  version = '$releasever'
when "amazon"
  version = '6'
end


yum_repository "zabbix" do
  url "http://repo.zabbix.com/zabbix/2.2/rhel/" + version + "/$basearch/"
  gpgkey "http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX"
  action :add
end

yum_repository "zabbix-non-supported" do
  url "http://repo.zabbix.com/non-supported/rhel/" + version + "/$basearch/"
  gpgkey "http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX"
  action :add
end
 
package "zabbix-agent" do
  action :upgrade
end
 
template "/etc/zabbix/zabbix_agentd.conf" do
  group "root"
  owner "root"
  source "zabbix_agentd.conf.erb"
  notifies :restart, 'service[zabbix-agent]'
end

template "/etc/zabbix/zabbix_agentd.d/userparameter_mysql.conf" do
  group "root"
  owner "root"
  source "userparameter_mysql.conf.erb"
  notifies :restart, 'service[zabbix-agent]'
end
 
service "zabbix-agent" do
  action [ :enable, :start]
  supports :status => true, :restart => true
end
