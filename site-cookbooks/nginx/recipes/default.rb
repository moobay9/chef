#
# Cookbook Name:: httpd
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute

ssl_path   = "/etc/nginx/ssl/"
ssl_status = ""

case node['platform']
when "centos"
  yum_repository "nginx" do
    url "http://nginx.org/packages/centos/$releasever/$basearch/"
    gpgkey "http://nginx.org/packages/keys/nginx_signing.key"
    action :add
  end
end

package "nginx" do
  action :install
end

if node['nginx']['ssl'] == "enable"
  vh = data_bag_item('nginx', node['nginx']['fqdn'])

  directory ssl_path do
    mode 0755
    owner "root"
    group "root"
    :create
  end

  %w{key cer}.each do |ssldata|
    file ssl_path + node['nginx']['fqdn'] + '.' + ssldata do
      content vh[ssldata]
      owner 'root'
      group 'root'
      mode '644'
    end
  end

  ssl_status = "_ssl"
end

case node['nginx']['type']
when "default"
  template_source = "nginx" + ssl_status + ".conf.erb"
when "streaming"
  template_source = "streaming.nginx" + ssl_status + ".conf.erb"
when "reverse"
  template_source = "reverse.nginx" + ssl_status + ".conf.erb"
end

template "/etc/nginx/nginx.conf" do
  source template_source
  owner "root"
  group "root"
  mode 0644
  variables(
    :fqdn        => node['nginx']['fqdn'],
    :realserver  => node['nginx']['realserver'], 
    :realport    => node['nginx']['realport'],
    :processes   => node['nginx']['processes'],
    :listen      => node['nginx']['listen'],
    :listen_ssl  => node['nginx']['listen_ssl'],
    :connections => node['nginx']['connections']
  )
  notifies :reload, 'service[nginx]'
end

directory '/var/cache/nginx' do
  mode 0755
  owner "nginx"
  group "nginx"
  :create
end

service "nginx" do
  supports :status => true, :restart => true, :reload => true
  subscribes :restart, resources(:template => "/etc/nginx/nginx.conf")
  action [:enable, :start]
end
