name             'vsftpd'
maintainer       'Funaffect Inc.'
maintainer_email 'm.oobayashi@funaffect.jp'
license          'All rights reserved'
description      'Installs/Configures vsftpd'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
