#
# Cookbook Name:: vsftpd
# Recipe:: default
#
# Copyright 2014, Funaffect
#
# All rights reserved - Do Not Redistribute
#

package "vsftpd" do
  action :install
end

template "/etc/vsftpd/vsftpd.conf" do
  source "vsftpd.conf.erb"
  owner "root"
  group "root"
  mode 0600
  notifies :restart, 'service[vsftpd]'
end

file '/etc/vsftpd/chroot_list' do
  owner "root"
  group "root"
  mode 0644
  action :create
end

service "vsftpd" do
  action [ :enable, :start ]
  subscribes :restart, resources(:template => "/etc/vsftpd/vsftpd.conf")
end
