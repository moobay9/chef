README
======

* Download

```
  curl -L https://www.chef.io/chef/install.sh | bash
```

* Setting

```
  yum update  
  yum install git  
  git clone git@bitbucket.org:moobay9/chef.git  
    または  
  git clone https://bitbucket.org/moobay9/chef.git  
```

* Chenge Directory  

```
  cd chef  
```

* Knife
  レシピを追加する場合  

```
knife cookbook site install yum -o cookbooks
knife cookbook site install yum-epel -o cookbooks
knife cookbook site install yum-remi -o cookbooks
```

* Run  

```
  chef-solo -c solo.rb -j ./localhost.json
```
